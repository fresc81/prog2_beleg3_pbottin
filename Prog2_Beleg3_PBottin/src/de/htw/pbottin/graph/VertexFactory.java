package de.htw.pbottin.graph;

import java.util.Random;

/**
 * This interface is used in combination with the Graph's createRandom method to randomly generate vertex id's.
 * 
 * @author Paul Bottin
 *
 * @param <V>
 *    the vertex id type
 */
public interface VertexFactory<V extends Comparable<V>>
{
  
  /**
   * Implementors use this method to randomly generate vertices for the graph.
   * 
   * @param rng
   *  a pseudo random number generator
   *  
   * @return
   *  a new vertex id
   */
  V createVertex(Random rng);
  
}