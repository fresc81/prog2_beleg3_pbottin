/**
 * 
 */
package de.htw.pbottin.graph;

import java.io.Serializable;
import java.util.AbstractCollection;
import java.util.AbstractList;
import java.util.AbstractSet;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;


/**
 * Provides some static methods to generate readonly filtered views of collections, lists and sets.
 * 
 * @author Paul Bottin
 *
 */
public class CollectionUtils
{
  
  /**
   * 
   */
  private CollectionUtils()
  {
  }
  
  /**
   * A simple filter that is applied to the collection.
   * 
   * @author Paul Bottin
   *
   * @param <T>
   *    the element type
   */
  public static interface Predicate<T> extends Serializable
  {
    
    /**
     * Checks if the element is visible through the filtered collection view.
     * 
     * @param value
     *  the element to test
     *  
     * @return
     *  True if the element is visible through the filtered collection view or false if not.
     */
    boolean apply(T value);
    
  }
  
  // a filtered collection wrapper
  private static class FilteredCollection<E> extends AbstractCollection<E> implements Serializable
  {
    
    /**
     * 
     */
    private static final long serialVersionUID = -346804489059966525L;

    private class FilteredCollectionIterator implements Iterator<E>
    {
      
      private final Iterator<? extends E> parentIterator = parentCollection.iterator();
      
      private E next = null;
      
      {
        advance();
      }
      
      @Override
      public boolean hasNext()
      {
        return next != null;
      }
      
      @Override
      public E next()
      {
        E result = next;
        if (result == null)
          throw new NoSuchElementException();
        next = advance();
        return result;
      }
      
      private E advance()
      {
        while (parentIterator.hasNext())
          if (predicate.apply(next = parentIterator.next()))
            return next;
          else
            next = null;
        return null;
      }
      
      @Override
      public void remove()
      {
        throw new UnsupportedOperationException();
      }
      
    }
    
    private final Collection<? extends E> parentCollection;
    private final Predicate<E> predicate;
    
    public FilteredCollection(Collection<? extends E> parentCollection, Predicate<E> predicate)
    {
      this.parentCollection = parentCollection;
      this.predicate = predicate;
    }
    
    @Override
    public Iterator<E> iterator()
    {
      return new FilteredCollectionIterator();
    }
    
    @Override
    public int size()
    {
      return count(iterator());
    }
    
  }
  
  // a filtered set wrapper
  private static class FilteredSet<E> extends AbstractSet<E> implements Serializable
  {
    
    /**
     * 
     */
    private static final long serialVersionUID = -6198507896318294765L;

    private class FilteredSetIterator implements Iterator<E>
    {
      
      private final Iterator<? extends E> parentIterator = parentSet.iterator();
      
      private E next = null;
      
      {
        advance();
      }
      
      @Override
      public boolean hasNext()
      {
        return next != null;
      }
      
      @Override
      public E next()
      {
        E result = next;
        if (result == null)
          throw new NoSuchElementException();
        next = advance();
        return result;
      }
      
      private E advance()
      {
        while (parentIterator.hasNext())
          if (predicate.apply(next = parentIterator.next()))
            return next;
          else
            next = null;
        return null;
      }
      
      @Override
      public void remove()
      {
        throw new UnsupportedOperationException();
      }
      
    }
    
    private final Set<? extends E> parentSet;
    private final Predicate<E> predicate;
    
    public FilteredSet(Set<? extends E> parentSet, Predicate<E> predicate)
    {
      this.parentSet = parentSet;
      this.predicate = predicate;
    }
    
    @Override
    public Iterator<E> iterator()
    {
      return new FilteredSetIterator();
    }
    
    @Override
    public int size()
    {
      return count(iterator());
    }
    
  }
  
  // a filtered list wrapper
  private static class FilteredList<E> extends AbstractList<E> implements Serializable
  {
    
    /**
     * 
     */
    private static final long serialVersionUID = 5006173415992210079L;

    private class FilteredListIterator implements Iterator<E>
    {
      
      private final Iterator<? extends E> parentIterator = parentList.iterator();
      
      private E next = null;
      
      {
        advance();
      }
      
      @Override
      public boolean hasNext()
      {
        return next != null;
      }
      
      @Override
      public E next()
      {
        E result = next;
        if (result == null)
          throw new NoSuchElementException();
        next = advance();
        return result;
      }
      
      private E advance()
      {
        while (parentIterator.hasNext())
          if (predicate.apply(next = parentIterator.next()))
            return next;
          else
            next = null;
        return null;
      }
      
      @Override
      public void remove()
      {
        throw new UnsupportedOperationException();
      }
      
    }
    
    private final List<? extends E> parentList;
    private final Predicate<E> predicate;
    
    public FilteredList(List<? extends E> parentList, Predicate<E> predicate)
    {
      this.parentList = parentList;
      this.predicate = predicate;
    }
    
    @Override
    public Iterator<E> iterator()
    {
      return new FilteredListIterator();
    }
    
    @Override
    public int size()
    {
      return count(iterator());
    }

    @Override
    public E get(int index)
    {
      return Collections.list(Collections.enumeration(this)).get(index);
    }
    
  }
  
  /**
   * Counts the number of elements the given iterator has left.
   * 
   * @param iterator
   *    the iterator whose number of elements should be counted
   * 
   * @return
   *    the number of elements
   */
  public static <E> int count(Iterator<? extends E> iterator)
  {
    int result = 0;
    while (iterator.hasNext())
    {
      ++result;
      iterator.next();
    }
    return result;
  }
  
  /**
   * Creates a readonly view of the given collection that is filtered by the given predicate.
   * 
   * @param collection
   *  the collection to filter
   * @param predicate
   *  the filter predicate
   * @return
   *  a new filtered collection
   */
  public static <E> Collection<E> createFilteredCollection(Collection<? extends E> collection, Predicate<E> predicate)
  {
    return Collections.unmodifiableCollection(new FilteredCollection<E>(collection, predicate));
  }
  
  /**
   * Creates a readonly view of the given set that is filtered by the given predicate.
   * 
   * @param set
   *  the set to filter
   * @param predicate
   *  the filter predicate
   * @return
   *  a new filtered set
   */
  public static <E> Set<E> createFilteredSet(Set<? extends E> set, Predicate<E> predicate)
  {
    return Collections.unmodifiableSet(new FilteredSet<E>(set, predicate));
  }
  
  /**
   * Creates a readonly view of the given list that is filtered by the given predicate.
   * 
   * @param list
   *  the list to filter
   * @param predicate
   *  the filter predicate
   * @return
   *  a new filtered list
   */
  public static <E> List<E> createFilteredList(List<? extends E> list, Predicate<E> predicate)
  {
    return Collections.unmodifiableList(new FilteredList<E>(list, predicate));
  }
  
}
