/**
 * 
 */
package de.htw.pbottin.graph;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * 
 * @author Paul Bottin
 * 
 * @param <V>
 *  the path element type
 *
 */
public class Path<V extends Comparable<V>> implements Comparable<Path<V>>
{
  
  private final List<V> pathSegments;
  
  // create root
  private Path(V rootSegment)
  {
    this.pathSegments = Collections.singletonList(rootSegment);
  }
  
  // create parent path
  private Path(Path<V> childPath)
  {
    this.pathSegments = Collections.unmodifiableList(childPath.pathSegments.subList(0, childPath.pathSegments.size()-1));
  }
  
  // create child path
  private Path(Path<V> parentPath, V childSegment)
  {
    final ArrayList<V> pathSegments = new ArrayList<>(parentPath.pathSegments.size()+1);
    pathSegments.addAll(parentPath.pathSegments);
    pathSegments.add(childSegment);
    this.pathSegments = Collections.unmodifiableList(pathSegments);
  }
  
  /**
   * Gets the list of path segments.
   * 
   * @return
   *    a readonly list of the path segments.
   */
  public List<V> getPathSegments()
  {
    return pathSegments;
  }
  
  /**
   * Gets the last paat segment.
   * 
   * @return
   *    the last element in the list of path segments
   */
  public V getCurrentSegment()
  {
    return pathSegments.get(pathSegments.size()-1);
  }
  
  /**
   * Creates a new path containing only the root segment.
   * 
   * @param rootSegment
   *    the root path segment
   *    
   * @return
   *    a new path object with the given root segment as it's only segment
   */
  public static <V extends Comparable<V>> Path<V> createRootPath(V rootSegment)
  {
    if (rootSegment == null)
      throw new NullPointerException();
    return new Path<>(rootSegment);
  }
  
  /**
   * Tests if this path object is a root path containing only one segment.
   * 
   * @return
   *    true if this path object is a root path, otherwise false is returned
   */
  public boolean isRoot()
  {
    return pathSegments.size() == 1;
  }
  
  /**
   * Creates a new path object that represents the parent's path.
   * 
   * @return
   *    a new path object that points to the parent's path
   */
  public Path<V> getParentPath()
  {
    if (isRoot())
      throw new UnsupportedOperationException();
    return new Path<>(this);
  }
  
  /**
   * Creates a new child path object by adding the child segment to this path.
   *  
   * @param childSegment
   *    the child segment to be added to this path's segments
   *    
   * @return
   *    a new path object that points to the child's path
   */
  public Path<V> getChildPath(V childSegment)
  {
    if (childSegment == null)
      throw new NullPointerException();
    if (pathSegments.contains(childSegment))
      throw new UnsupportedOperationException();
    return new Path<>(this, childSegment);
  }

  /*
   * (non-Javadoc)
   * @see java.lang.Comparable#compareTo(java.lang.Object)
   */
  @Override
  public int compareTo(Path<V> o)
  {
    int len = pathSegments.size();
    int olen = o.pathSegments.size();
    final int n = Math.min(len, olen);
    
    for (int i = 0; i < n; i++)
    {
      V a = pathSegments.get(i);
      V b = o.pathSegments.get(i);
      int m = a.compareTo(b);
      
      if (m < 0)
        return -1;
      
      if (m > 0)
        return 1;
      
    }
    
    if (len < olen)
      return -1;
    
    if (len > olen)
      return 1;
    
    return 0;
  }
  
  /*
   * (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString()
  {
    StringBuilder result = new StringBuilder();
    
    for (V segment : pathSegments)
    {
      result.append('/');
      result.append(segment.toString());
    }
    
    return result.toString();
  }
  
}
