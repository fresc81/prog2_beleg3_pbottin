package de.htw.pbottin.graph.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;


/**
 * @author Paul Bottin
 *
 */
public class SettingsDialog extends JDialog
{
  
  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * @author Paul Bottin
   *
   */
  public static class Settings
  {
    
    private Dimension graphDisplayDimension;
    
    /**
     * @param graphDisplayDimension
     */
    public Settings(Dimension graphDisplayDimension)
    {
      this.graphDisplayDimension = graphDisplayDimension;
    }
    
    /**
     * @return
     */
    public Dimension getGraphDisplayDimension()
    {
      return graphDisplayDimension;
    }
    
    /**
     * @param graphDisplayDimension
     */
    public void setGraphDisplayDimension(Dimension graphDisplayDimension)
    {
      this.graphDisplayDimension = graphDisplayDimension;
    }
    
  }
  
  private final JPanel contentPanel = new JPanel();
  
  private JSpinner widthSpinner;
  private JSpinner heightSpinner;
  
  private Action pressedButton = null;
  
  private final Action okAction = new OkAction();
  private final Action cancelAction = new CancelAction();
  
  /**
   * Create the dialog.
   * @param graphEditorFrame 
   */
  public SettingsDialog(GraphEditorFrame graphEditorFrame)
  {
    super(graphEditorFrame);
    initialize();
  }

  private void initialize()
  {
    setTitle("Settings");
    setModal(true);
    setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
    setBounds(100, 100, 450, 159);
    getContentPane().setLayout(new BorderLayout());
    contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
    getContentPane().add(contentPanel, BorderLayout.CENTER);
    contentPanel.setLayout(new BorderLayout(0, 0));
    {
      JPanel graphDisplayPanel = new JPanel();
      graphDisplayPanel.setBorder(new TitledBorder(null, "Graph Display", TitledBorder.LEADING, TitledBorder.TOP, null, null));
      contentPanel.add(graphDisplayPanel, BorderLayout.NORTH);
      GridBagLayout gbl_graphDisplayPanel = new GridBagLayout();
      gbl_graphDisplayPanel.columnWidths = new int[] {100, 0};
      gbl_graphDisplayPanel.rowHeights = new int[] {0};
      gbl_graphDisplayPanel.columnWeights = new double[]{0.0, 1.0};
      gbl_graphDisplayPanel.rowWeights = new double[]{0.0, 0.0};
      graphDisplayPanel.setLayout(gbl_graphDisplayPanel);
      {
        JLabel lblWidth = new JLabel("Width:");
        GridBagConstraints gbc_lblWidth = new GridBagConstraints();
        gbc_lblWidth.anchor = GridBagConstraints.WEST;
        gbc_lblWidth.insets = new Insets(0, 0, 5, 5);
        gbc_lblWidth.gridx = 0;
        gbc_lblWidth.gridy = 0;
        graphDisplayPanel.add(lblWidth, gbc_lblWidth);
      }
      {
        widthSpinner = new JSpinner();
        GridBagConstraints gbc_widthSpinner = new GridBagConstraints();
        gbc_widthSpinner.insets = new Insets(0, 0, 5, 0);
        gbc_widthSpinner.fill = GridBagConstraints.HORIZONTAL;
        gbc_widthSpinner.gridx = 1;
        gbc_widthSpinner.gridy = 0;
        graphDisplayPanel.add(widthSpinner, gbc_widthSpinner);
      }
      {
        JLabel lblHeight = new JLabel("Height:");
        GridBagConstraints gbc_lblHeight = new GridBagConstraints();
        gbc_lblHeight.insets = new Insets(0, 0, 0, 5);
        gbc_lblHeight.anchor = GridBagConstraints.WEST;
        gbc_lblHeight.gridx = 0;
        gbc_lblHeight.gridy = 1;
        graphDisplayPanel.add(lblHeight, gbc_lblHeight);
      }
      {
        heightSpinner = new JSpinner();
        GridBagConstraints gbc_heightSpinner = new GridBagConstraints();
        gbc_heightSpinner.fill = GridBagConstraints.HORIZONTAL;
        gbc_heightSpinner.gridx = 1;
        gbc_heightSpinner.gridy = 1;
        graphDisplayPanel.add(heightSpinner, gbc_heightSpinner);
      }
    }
    {
      JPanel buttonPane = new JPanel();
      buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
      getContentPane().add(buttonPane, BorderLayout.SOUTH);
      {
        JButton okButton = new JButton("OK");
        okButton.setAction(okAction);
        okButton.setActionCommand("OK");
        buttonPane.add(okButton);
        getRootPane().setDefaultButton(okButton);
      }
      {
        JButton cancelButton = new JButton("Cancel");
        cancelButton.setAction(cancelAction);
        cancelButton.setActionCommand("Cancel");
        buttonPane.add(cancelButton);
      }
    }
  }
  
  private class OkAction extends AbstractAction {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    public OkAction() {
      putValue(NAME, "Ok");
    }
    public void actionPerformed(ActionEvent e) {
      pressedButton = this;
      setVisible(false);
    }
  }
  
  private class CancelAction extends AbstractAction {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    public CancelAction() {
      putValue(NAME, "Cancel");
    }
    public void actionPerformed(ActionEvent e) {
      pressedButton = this;
      setVisible(false);
    }
  }
  
  public Settings showDialog(Settings settings)
  {
    Settings newSettings = null;
    
    if (settings != null)
    {
      Dimension graphDisplayDimension = settings.getGraphDisplayDimension();
      this.widthSpinner.setValue(graphDisplayDimension.width);
      this.heightSpinner.setValue(graphDisplayDimension.height);
    }
    
    this.pressedButton = null;
    this.setModal(true);
    this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    this.setVisible(true);
    
    if (this.pressedButton == okAction)
      newSettings = new Settings(new Dimension((Integer)widthSpinner.getValue(), (Integer)heightSpinner.getValue()));
    
    return newSettings;
  }
  
}
