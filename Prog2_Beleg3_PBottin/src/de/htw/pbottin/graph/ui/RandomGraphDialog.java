package de.htw.pbottin.graph.ui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.GridBagLayout;
import javax.swing.JLabel;
import java.awt.GridBagConstraints;
import javax.swing.JSpinner;
import java.awt.Insets;
import javax.swing.AbstractAction;
import java.awt.event.ActionEvent;
import javax.swing.Action;


/**
 * A dialog for user input of the parameters nessesary to generate a random graph.
 * 
 * @author Paul Bottin
 *
 */
public class RandomGraphDialog extends JDialog
{
  
  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Stores the data for this dialog.
   * 
   * @author Paul Bottin
   *
   */
  public static class RandomGraphSettings
  {
    
    private int numVertices;
    
    private int numEdges;

    /**
     * @param numVertices
     * @param numEdges
     */
    public RandomGraphSettings(int numVertices, int numEdges)
    {
      this.numVertices = numVertices;
      this.numEdges = numEdges;
    }

    
    /**
     * @return the numVertices
     */
    public int getNumVertices()
    {
      return numVertices;
    }

    
    /**
     * @param numVertices the numVertices to set
     */
    public void setNumVertices(int numVertices)
    {
      this.numVertices = numVertices;
    }

    
    /**
     * @return the numEdges
     */
    public int getNumEdges()
    {
      return numEdges;
    }

    
    /**
     * @param numEdges the numEdges to set
     */
    public void setNumEdges(int numEdges)
    {
      this.numEdges = numEdges;
    }
    
    
    
  }
  
  private final JPanel contentPanel = new JPanel();
  
  private Action pressedButton = null;
  
  private final Action okAction = new OkAction();
  private final Action cancelAction = new CancelAction();
  private JSpinner numVerticesSpinner;
  private JSpinner numEdgesSpinner;
  
  /**
   * Create the dialog.
   * 
   * @param graphEditorFrame 
   */
  public RandomGraphDialog(GraphEditorFrame graphEditorFrame)
  {
    super(graphEditorFrame);
    initialize();
  }

  /**
   * 
   */
  private void initialize()
  {
    setTitle("Random Graph Parameters");
    setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
    setModal(true);
    setBounds(100, 100, 450, 128);
    getContentPane().setLayout(new BorderLayout());
    contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
    getContentPane().add(contentPanel, BorderLayout.CENTER);
    GridBagLayout gbl_contentPanel = new GridBagLayout();
    gbl_contentPanel.columnWidths = new int[]{0, 0, 0};
    gbl_contentPanel.rowHeights = new int[]{0, 0, 0};
    gbl_contentPanel.columnWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
    gbl_contentPanel.rowWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
    contentPanel.setLayout(gbl_contentPanel);
    {
      JLabel lblNumberOfVertices = new JLabel("number of vertices:");
      GridBagConstraints gbc_lblNumberOfVertices = new GridBagConstraints();
      gbc_lblNumberOfVertices.anchor = GridBagConstraints.EAST;
      gbc_lblNumberOfVertices.insets = new Insets(0, 0, 5, 5);
      gbc_lblNumberOfVertices.gridx = 0;
      gbc_lblNumberOfVertices.gridy = 0;
      contentPanel.add(lblNumberOfVertices, gbc_lblNumberOfVertices);
    }
    {
      numVerticesSpinner = new JSpinner();
      GridBagConstraints gbc_numVerticesSpinner = new GridBagConstraints();
      gbc_numVerticesSpinner.fill = GridBagConstraints.HORIZONTAL;
      gbc_numVerticesSpinner.insets = new Insets(0, 0, 5, 0);
      gbc_numVerticesSpinner.gridx = 1;
      gbc_numVerticesSpinner.gridy = 0;
      contentPanel.add(numVerticesSpinner, gbc_numVerticesSpinner);
    }
    {
      JLabel lblNumberOfEdges = new JLabel("number of edges:");
      GridBagConstraints gbc_lblNumberOfEdges = new GridBagConstraints();
      gbc_lblNumberOfEdges.anchor = GridBagConstraints.EAST;
      gbc_lblNumberOfEdges.insets = new Insets(0, 0, 0, 5);
      gbc_lblNumberOfEdges.gridx = 0;
      gbc_lblNumberOfEdges.gridy = 1;
      contentPanel.add(lblNumberOfEdges, gbc_lblNumberOfEdges);
    }
    {
      numEdgesSpinner = new JSpinner();
      GridBagConstraints gbc_numEdgesSpinner = new GridBagConstraints();
      gbc_numEdgesSpinner.fill = GridBagConstraints.HORIZONTAL;
      gbc_numEdgesSpinner.gridx = 1;
      gbc_numEdgesSpinner.gridy = 1;
      contentPanel.add(numEdgesSpinner, gbc_numEdgesSpinner);
    }
    {
      JPanel buttonPane = new JPanel();
      buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
      getContentPane().add(buttonPane, BorderLayout.SOUTH);
      {
        JButton okButton = new JButton("OK");
        okButton.setAction(okAction);
        okButton.setActionCommand("OK");
        buttonPane.add(okButton);
        getRootPane().setDefaultButton(okButton);
      }
      {
        JButton cancelButton = new JButton("Cancel");
        cancelButton.setAction(cancelAction);
        cancelButton.setActionCommand("Cancel");
        buttonPane.add(cancelButton);
      }
    }
  }
  
  private class OkAction extends AbstractAction {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    public OkAction() {
      putValue(NAME, "Ok");
    }
    public void actionPerformed(ActionEvent e) {
      pressedButton = this;
      setVisible(false);
    }
  }
  
  private class CancelAction extends AbstractAction {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    public CancelAction() {
      putValue(NAME, "Cancel");
    }
    public void actionPerformed(ActionEvent e) {
      pressedButton = this;
      setVisible(false);
    }
  }
  
  /**
   * @param settings
   * @return
   */
  public RandomGraphSettings showDialog(RandomGraphSettings settings)
  {
    RandomGraphSettings newSettings = null;
    
    if (settings != null)
    {
      this.numVerticesSpinner.setValue((Integer) settings.numVertices);
      this.numEdgesSpinner.setValue((Integer) settings.numEdges);
    }
    
    this.pressedButton = null;
    this.setModal(true);
    this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    this.setVisible(true);
    
    if (this.pressedButton == okAction)
      newSettings = new RandomGraphSettings((Integer)numVerticesSpinner.getValue(), (Integer)numEdgesSpinner.getValue());
    
    return newSettings;
  }
  
}
