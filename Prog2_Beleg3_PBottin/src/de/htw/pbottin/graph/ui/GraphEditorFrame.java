package de.htw.pbottin.graph.ui;


import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ComponentEvent;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;

import javax.imageio.ImageIO;
import javax.swing.AbstractAction;
import javax.swing.AbstractListModel;
import javax.swing.Action;
import javax.swing.DefaultListModel;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JToolBar;
import javax.swing.ListSelectionModel;
import javax.swing.border.EtchedBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.filechooser.FileFilter;

import de.htw.pbottin.graph.EdgeFactory;
import de.htw.pbottin.graph.Graph;
import de.htw.pbottin.graph.Path;
import de.htw.pbottin.graph.VertexFactory;
import de.htw.pbottin.graph.ui.RandomGraphDialog.RandomGraphSettings;
import de.htw.pbottin.graph.ui.SettingsDialog.Settings;


/**
 * @author Paul Bottin
 *
 */
public class GraphEditorFrame extends JFrame
{
  
  /**
   * 
   */
  private static final long serialVersionUID = -2257200909087338965L;
  
  private static final String ICON_GRAPH = "graph.png";
  
  private static final String ICON_GRAPH_NEW = "document-new.png";
  
  private static final String ICON_GRAPH_OPEN = "document-open.png";
  
  private static final String ICON_GRAPH_SAVE = "document-save.png";
  
  private static final String ICON_GRAPH_SAVE_AS = "document-save-as.png";
  
  private static final String ICON_GRAPH_RANDOM = "random.png";
  
  private static final String ICON_GRAPH_EXPORT_PNG = "document-export.png";
  
  private static final String ICON_SETTINGS = "settings.png";
  
  private static final String ICON_VERTEX_CREATE = "node-add.png";
  
  private static final String ICON_VERTEX_DELETE = "node-remove.png";
  
  private static final String ICON_EDGE_SET = "add.png";
  
  private static final String ICON_EDGE_REMOVE = "delete.png";
  
  private static final String ICON_PATHFIND = "find.png";
  
  private class VertexListModel extends AbstractListModel<Graph<String, Double>.Vertex>
  {
    
    /**
     * 
     */
    private static final long serialVersionUID = -6770253488851718019L;

    @Override
    public int getSize()
    {
      return graph.getVertices().size();
    }
    
    @Override
    public Graph<String, Double>.Vertex getElementAt(int index)
    {
      Graph<String, Double>.Vertex vertex = null;
      Iterator<Graph<String, Double>.Vertex> iterator = graph.getVertices().iterator();
      
      int i = 0;
      while ((vertex == null) && iterator.hasNext())
      {
        Graph<String, Double>.Vertex current = iterator.next();
        if (i == index)
          vertex = current;
        ++i;
      }
      
      return vertex;
    }
    
    @Override
    protected void fireContentsChanged(Object source, int index0, int index1)
    {
      super.fireContentsChanged(source, index0, index1);
    }
    
    @Override
    protected void fireIntervalAdded(Object source, int index0, int index1)
    {
      super.fireIntervalAdded(source, index0, index1);
    }
    
    @Override
    protected void fireIntervalRemoved(Object source, int index0, int index1)
    {
      super.fireIntervalRemoved(source, index0, index1);
    }
    
  }
  
  private class EdgeListModel extends AbstractListModel<Graph<String, Double>.Edge>
  {

    /**
     * 
     */
    private static final long serialVersionUID = 5084954147788478645L;

    @Override
    public int getSize()
    {
      return graph.getEdges().size();
    }

    @Override
    public Graph<String, Double>.Edge getElementAt(int index)
    {
      Graph<String, Double>.Edge edge = null;
      Iterator<Graph<String, Double>.Edge> iterator = graph.getEdges().iterator();
      
      int i = 0;
      while ((edge == null) && iterator.hasNext())
      {
        Graph<String, Double>.Edge current = iterator.next();
        if (i == index)
          edge = current;
        ++i;
      }
      
      return edge;
    }
    
    @Override
    protected void fireContentsChanged(Object source, int index0, int index1)
    {
      super.fireContentsChanged(source, index0, index1);
    }
    
    @Override
    protected void fireIntervalAdded(Object source, int index0, int index1)
    {
      super.fireIntervalAdded(source, index0, index1);
    }
    
    @Override
    protected void fireIntervalRemoved(Object source, int index0, int index1)
    {
      super.fireIntervalRemoved(source, index0, index1);
    }
    
  }
  
  private Graph<String, Double> graph;
  
  private JToolBar toolbar;
  
  private JList<Graph<String, Double>.Vertex> vertexList;
  
  private JList<Graph<String, Double>.Edge> edgeList;
  
  private VertexListModel vertexListModel = new VertexListModel();
  
  private EdgeListModel edgeListModel = new EdgeListModel();
  
  private JList<Graph<String, Double>.Vertex> connectedVerticesList;
  
  private JList<Graph<String, Double>.Edge> connectedEdgesList;
  
  private DefaultListModel<Graph<String, Double>.Vertex> connectedVerticesListModel = new DefaultListModel<Graph<String,Double>.Vertex>();
  
  private DefaultListModel<Graph<String, Double>.Edge> connectedEdgesListModel = new DefaultListModel<Graph<String,Double>.Edge>();
  
  private BufferedImage image;
  
  private JPanel centerPanel;
  
  private JLabel graphCanvas;
  
  private Settings settings;
  
  private SettingsDialog settingsDialog;
  
  private RandomGraphSettings randomGraphSettings;
  
  private RandomGraphDialog randomGraphDialog;
  
  private File graphFile;
  
  private Action newGraphAction = new AbstractAction("new Graph", loadIcon(ICON_GRAPH_NEW)) {
    
    /**
     * 
     */
    private static final long serialVersionUID = -3845829294684162593L;
    
    {
      putValue(SHORT_DESCRIPTION, "Create new Graph");
    }
    
    @Override
    public void actionPerformed(ActionEvent e)
    {
      int numVertices = Math.max(0, graph.getVertices().size()-1),
          numEdges =  Math.max(0, graph.getEdges().size()-1);
      
      graph = new Graph<>();
      vertexListModel.fireIntervalRemoved(vertexListModel, 0, numVertices);
      edgeListModel.fireIntervalRemoved(edgeListModel, 0, numEdges);
      graphFile = null;
      GraphEditorFrame.this.setTitle("Graph Editor");
      saveGraphAction.setEnabled(false);
      repaintCanvas();
    }
    
  };
  
  private Action loadGraphAction = new AbstractAction("open Graph...", loadIcon(ICON_GRAPH_OPEN)) {
    
    /**
     * 
     */
    private static final long serialVersionUID = -7020725692724098500L;
    
    {
      putValue(SHORT_DESCRIPTION, "Load existing Graph from a file");
    }
    
    @Override
    public void actionPerformed(ActionEvent e)
    {
      int numVertices = Math.max(0, graph.getVertices().size()-1),
          numEdges =  Math.max(0, graph.getEdges().size()-1);
      
      JFileChooser fileChooser = new JFileChooser();
      fileChooser.setMultiSelectionEnabled(false);
      if (graphFile != null) fileChooser.setSelectedFile(graphFile);
      if (fileChooser.showOpenDialog(GraphEditorFrame.this) == JFileChooser.APPROVE_OPTION)
      {
        graphFile = fileChooser.getSelectedFile();
        
        if (graphFile != null)
        {
          try
          {
            FileInputStream in = new FileInputStream(graphFile);
            graph = Graph.loadGraph(in);
            in.close();
            
            vertexListModel.fireIntervalRemoved(vertexListModel, 0, numVertices);
            edgeListModel.fireIntervalRemoved(edgeListModel, 0, numEdges);
            
            vertexListModel.fireIntervalAdded(vertexListModel, 0, graph.getVertices().size());
            edgeListModel.fireIntervalAdded(edgeListModel, 0, graph.getEdges().size());
            
            repaintCanvas();
            GraphEditorFrame.this.setTitle("Graph Editor - "+graphFile.toString());
            saveGraphAction.setEnabled(true);
            
          } catch (IOException | ClassNotFoundException ex)
          {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(GraphEditorFrame.this, ex.getLocalizedMessage());
          }
        }
      }
    }
    
  };
  
  private Action saveGraphAction = new AbstractAction("save Graph", loadIcon(ICON_GRAPH_SAVE)) {
    
    /**
     * 
     */
    private static final long serialVersionUID = 5972227106444608159L;
    
    {
      setEnabled(false);
      putValue(SHORT_DESCRIPTION, "Save Graph");
    }

    @Override
    public void actionPerformed(ActionEvent e)
    {
      if (graphFile != null)
      {
        graphFile = graphFile.getAbsoluteFile();
        if (!graphFile.exists() || (JOptionPane.showConfirmDialog(GraphEditorFrame.this, "File already exists, do you want to overwrite it?", "confirm overwrite", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION))
          try
          {
            FileOutputStream out = new FileOutputStream(graphFile);
            graph.storeGraph(out);
            out.close();
            
            GraphEditorFrame.this.setTitle("Graph Editor - "+graphFile.toString());
            saveGraphAction.setEnabled(true);
          } catch (IOException ex)
          {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(GraphEditorFrame.this, ex.getLocalizedMessage());
          }
      }
    }
    
  };
  
  private Action saveAsGraphAction = new AbstractAction("save Graph as...", loadIcon(ICON_GRAPH_SAVE_AS)) {
    
    /**
     * 
     */
    private static final long serialVersionUID = -5285977168203034207L;
    
    {
      putValue(SHORT_DESCRIPTION, "Save Graph under a new filename");
    }
    
    
    @Override
    public void actionPerformed(ActionEvent e)
    {
      JFileChooser fileChooser = new JFileChooser();
      fileChooser.setMultiSelectionEnabled(false);
      if (graphFile != null) fileChooser.setSelectedFile(graphFile);
      if (fileChooser.showSaveDialog(GraphEditorFrame.this) == JFileChooser.APPROVE_OPTION)
      {
        graphFile = fileChooser.getSelectedFile();
        if (graphFile != null)
        {
          graphFile = graphFile.getAbsoluteFile();
          if (!graphFile.exists() || (JOptionPane.showConfirmDialog(GraphEditorFrame.this, "File already exists, do you want to overwrite it?", "confirm overwrite", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION))
            try
            {
              FileOutputStream out = new FileOutputStream(graphFile);
              graph.storeGraph(out);
              out.close();

              GraphEditorFrame.this.setTitle("Graph Editor - "+graphFile.toString());
              saveGraphAction.setEnabled(true);
            } catch (IOException ex)
            {
              ex.printStackTrace();
              JOptionPane.showMessageDialog(GraphEditorFrame.this, ex.getLocalizedMessage());
            }
        }
      }
    }
    
  };
  
  private Action exportPNGAction = new AbstractAction("export Graph to PNG...", loadIcon(ICON_GRAPH_EXPORT_PNG)) {
    
    /**
     * 
     */
    private static final long serialVersionUID = -5007847966209957578L;
    
    {
      putValue(SHORT_DESCRIPTION, "Export the current Graph to a PNG image file");
    }
    
    @Override
    public void actionPerformed(ActionEvent e)
    {
      JFileChooser fileChooser = new JFileChooser();
      fileChooser.setMultiSelectionEnabled(false);
      fileChooser.setFileFilter(new FileFilter()
      {
        
        @Override
        public String getDescription()
        {
          return "*.png";
        }
        
        @Override
        public boolean accept(File file)
        {
          return fileGetSuffix(file).equalsIgnoreCase("png");
        }
        
      });
      
      fileChooser.showSaveDialog(GraphEditorFrame.this);
      File file = fileChooser.getSelectedFile();
      try
      {
        ImageIO.write(image, "png", file);
      } catch (IOException ex)
      {
        ex.printStackTrace();
      }
    }
    
  };
  
  private Action randomGraphAction = new AbstractAction("new random Graph...", loadIcon(ICON_GRAPH_RANDOM)) {
    
    /**
     * 
     */
    private static final long serialVersionUID = 6188073951857273070L;
    
    {
      putValue(SHORT_DESCRIPTION, "Randomly generates a new Graph");
    }
    
    @Override
    public void actionPerformed(ActionEvent e)
    {
      
      RandomGraphSettings newSettings = queryRandomGraphSettings(randomGraphSettings);
      if (newSettings != null)
      {
        randomGraphSettings = newSettings;
        
        VertexFactory<String> vertexFactory = new VertexFactory<String>()
        {
          
          int numGenerated = 0;
          
          @Override
          public String createVertex(Random rng)
          {
            char chr = (char) ('A' + numGenerated);
            ++numGenerated;
            return Character.toString(chr);
          }
          
        };
        
        EdgeFactory<String, Double> edgeFactory = new EdgeFactory<String, Double>()
        {
  
          @Override
          public Double createEdge(Random rng, Graph<String, Double>.Vertex src, Graph<String, Double>.Vertex dst)
          {
            return rng.nextDouble();
          }
          
        };
        
        int
          numVertices = Math.max(0, graph.getVertices().size()-1),
          numEdges =  Math.max(0, graph.getEdges().size()-1);
        
        graph = new Graph<>();
        vertexListModel.fireIntervalRemoved(vertexListModel, 0, numVertices);
        edgeListModel.fireIntervalRemoved(edgeListModel, 0, numEdges);
        
        Random rng = new Random();      
        graph = Graph.createRandom(rng, newSettings.getNumVertices(), vertexFactory, newSettings.getNumEdges(), edgeFactory);
        vertexListModel.fireIntervalAdded(vertexListModel, 0, newSettings.getNumVertices()-1);
        edgeListModel.fireIntervalAdded(edgeListModel, 0, newSettings.getNumEdges()-1);
        
        repaintCanvas();
      }
    }
    
  };
  
  private Action settingsAction = new AbstractAction("Settings...", loadIcon(ICON_SETTINGS)) {
    
    /**
     * 
     */
    private static final long serialVersionUID = -5007847966209957578L;
    
    {
      putValue(SHORT_DESCRIPTION, "Opens a dialog where the application settings can be set");
    }
    
    @Override
    public void actionPerformed(ActionEvent e)
    {
      Settings newSettings = querySettings(settings);
      if (newSettings != null)
      {
        
        settings = newSettings;
        Dimension dimension = settings.getGraphDisplayDimension();
        image = new BufferedImage(dimension.width, dimension.height, BufferedImage.TYPE_INT_RGB);
        graphCanvas.setIcon(new ImageIcon(image));
        
        repaintCanvas();
      }
    }
    
  };
  
  private Action createVertexAction = new AbstractAction("create Vertex...", loadIcon(ICON_VERTEX_CREATE)) {
    
    /**
     * 
     */
    private static final long serialVersionUID = -537157082468935493L;
    
    {
      putValue(SHORT_DESCRIPTION, "Creates a new vertex in the graph");
    }
    
    @Override
    public void actionPerformed(ActionEvent e)
    {
      String vertexId = JOptionPane.showInputDialog(GraphEditorFrame.this, "vertex id?", "Enter Name");
      Object vertex = graph.createVertex(vertexId);
      if (vertex != null)
      {
        int index = vertexListModel.getSize();
        vertexListModel.fireIntervalAdded(vertexListModel, index, index);
        repaintCanvas();
      }
    }
    
  };
  
  private Action deleteVertexAction = new AbstractAction("delete Vertex", loadIcon(ICON_VERTEX_DELETE))
  {
    
    /**
     * 
     */
    private static final long serialVersionUID = -8817011702087950893L;
    
    {
      putValue(SHORT_DESCRIPTION, "Removes the currently selected vertex and connected edges from the graph");
    }
    
    @Override
    public void actionPerformed(ActionEvent e)
    {
      int index = vertexList.getSelectedIndex();
      Graph<String, Double>.Vertex vertex = vertexList.getSelectedValue();
      if (vertex != null)
      {
        graph.deleteVertex(vertex);
        vertexListModel.fireIntervalRemoved(vertexListModel, index, index);
        edgeListModel.fireContentsChanged(edgeListModel, 0, edgeListModel.getSize());
        repaintCanvas();
      }
    }
    
  };
  
  private Action createEdgeAction = new AbstractAction("set Edge...", loadIcon(ICON_EDGE_SET))
  {
    
    /**
     * 
     */
    private static final long serialVersionUID = 817389288930061044L;
    
    {
      putValue(SHORT_DESCRIPTION, "Allows to add or modify the edge weight");
    }
    
    @Override
    public void actionPerformed(ActionEvent e)
    {
      Graph<String, Double>.Vertex v1 = queryVertex("First vertex?", null);
      if (v1 != null)
      {
        Graph<String, Double>.Vertex v2 = queryVertex("Second vertex?", null);
        if (v2 != null)
        {
          double weight = Double.parseDouble(JOptionPane.showInputDialog(GraphEditorFrame.this, "weight ?"));
          
          Graph<String, Double>.Edge edge = graph.setEdge(v1, v2, weight == 0 ? null : weight);
          if (edge.getWeight() != null)
          {
            int index = edgeListModel.getSize();
            edgeListModel.fireIntervalAdded(edgeListModel, index, index);
            repaintCanvas();
          } else
          {
            int index = edgeListModel.getSize();
            edgeListModel.fireIntervalRemoved(edgeListModel, index, index);
            repaintCanvas();
          }
        }
      }
    }
    
  };
  
  private Action deleteEdgeAction = new AbstractAction("remove Edge", loadIcon(ICON_EDGE_REMOVE))
  {
    
    /**
     * 
     */
    private static final long serialVersionUID = -5299994345611184330L;
    
    {
      putValue(SHORT_DESCRIPTION, "Removes the currently selected edge from the graph");
    }
    
    @Override
    public void actionPerformed(ActionEvent e)
    {
      int index = edgeList.getSelectedIndex();
      Graph<String, Double>.Edge edge = edgeList.getSelectedValue();
      if (edge != null)
      {
        graph.removeEdge(edge);
        edgeListModel.fireIntervalRemoved(edgeListModel, index, index);
        repaintCanvas();
      }
    }
    
  };
  
  /*
  private Action dfsAction = new AbstractAction("depth first search")
  {
    
    
    @Override
    public void actionPerformed(ActionEvent e)
    {
      Collection<Graph<String, Double>.Vertex> verticesCollection = graph.getVertices();
      Object[] verticesArray = verticesCollection.toArray();
      
      int id1 = JOptionPane.showOptionDialog(GraphEditorFrame.this, "first vertex ?", "Enter Vertex", JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE, null, verticesArray, verticesArray[0]);
      if (id1 < 0) return;
      
      @SuppressWarnings("unchecked")
      Graph<String, Double>.Vertex v1 = (Graph<String, Double>.Vertex) verticesArray[id1];
      
      int id2 = JOptionPane.showOptionDialog(GraphEditorFrame.this, "second vertex ?", "Enter Vertex", JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE, null, verticesArray, verticesArray[0]);
      if (id2 < 0) return;
      
      @SuppressWarnings("unchecked")
      Graph<String, Double>.Vertex v2 = (Graph<String, Double>.Vertex) verticesArray[id2];
      
      System.out.println("==============[DepthFirstSearch]==============");
      Double smallestSum = null;
      Path<Graph<String, Double>.Vertex> shortestPath = null;
      Set<Path<Graph<String, Double>.Vertex>> paths = graph.depthFirstSearch(v1, v2);
      for (Path<Graph<String, Double>.Vertex> path : paths)
      {
        double sum = 0;
        System.out.println("PATH: " + path);
        List<Graph<String, Double>.Edge> edges = graph.getVertexPathEdges(path);
        for (Graph<String, Double>.Edge edge : edges)
        {
          System.out.println("EDGE: " + edge);
          sum += edge.getWeight();
        }
        System.out.println("SUM: " + sum);
        if ((smallestSum == null) || (smallestSum > sum))
        {
          System.out.println("is smaller");
          shortestPath = path;
          smallestSum = sum;
        }
      }
      
      if (smallestSum != null)
      {
        System.out.println("SHORTEST PATH ("+smallestSum+"): "+shortestPath);
        JOptionPane.showMessageDialog(GraphEditorFrame.this, "The shortest path with a total length of "+smallestSum+" is '"+shortestPath+"'.", "shortest Path was found", JOptionPane.INFORMATION_MESSAGE);
      } else {
        System.out.println("NO PATH");
        JOptionPane.showMessageDialog(GraphEditorFrame.this, "There is no valid path from '"+v1+"' to '"+v2+"'.", "no Path was found", JOptionPane.ERROR_MESSAGE);
      }
    }
    
  };
  */
  
  private Action bfsAction = new AbstractAction("find shortest path...", loadIcon(ICON_PATHFIND))
  {
    
    /**
     * 
     */
    private static final long serialVersionUID = -256462763074335834L;
    
    {
      putValue(SHORT_DESCRIPTION, "Finds the shortest path between two vertices");
    }
    
    @Override
    public void actionPerformed(ActionEvent e)
    {
      Graph<String, Double>.Vertex v1 = queryVertex("First vertex?", null);
      if (v1 != null)
      {
        Graph<String, Double>.Vertex v2 = queryVertex("Second vertex?", null);
        if (v2 != null)
        {
          
          System.out.println("==============[BreadthFirstSearch]==============");
          Double smallestSum = null;
          Path<Graph<String, Double>.Vertex> shortestPath = null;
          Set<Path<Graph<String, Double>.Vertex>> paths = graph.breadthFirstSearch(v1, v2);
          for (Path<Graph<String, Double>.Vertex> path : paths)
          {
            double sum = 0;
            System.out.println("PATH: " + path);
            List<Graph<String, Double>.Edge> edges = graph.getVertexPathEdges(path);
            for (Graph<String, Double>.Edge edge : edges)
            {
              System.out.println("EDGE: " + edge);
              sum += edge.getWeight();
            }
            System.out.println("SUM: " + sum);
            if ((smallestSum == null) || (smallestSum > sum))
            {
              System.out.println("is smaller");
              shortestPath = path;
              smallestSum = sum;
            }
          }
          
          if (smallestSum != null)
          {
            System.out.println("SHORTEST PATH (" + smallestSum + "): " + shortestPath);
            JOptionPane.showMessageDialog(GraphEditorFrame.this, "The shortest path with a total length of " + smallestSum + " is '" + shortestPath + "'.", "shortest Path was found", JOptionPane.INFORMATION_MESSAGE);
          } else
          {
            System.out.println("NO PATH");
            JOptionPane.showMessageDialog(GraphEditorFrame.this, "There is no valid path from '" + v1 + "' to '" + v2 + "'.", "no Path was found", JOptionPane.ERROR_MESSAGE);
          }
          
        }
      }
    }
    
  };
  
  /**
   * 
   * @param resourceName
   * @return
   */
  private static Image loadImage(String resourceName)
  {
    return Toolkit.getDefaultToolkit().getImage(GraphEditorFrame.class.getResource(resourceName));
  }
  
  /**
   * 
   * @param resourceName
   * @return
   */
  private static Icon loadIcon(String resourceName)
  {
    return new ImageIcon(loadImage(resourceName));    
  }
  
  /**
   * 
   * @param file
   * @return
   */
  private static String fileGetSuffix(File file)
  {
    String filename = file.getName();
    int dotPos = filename.lastIndexOf('.');
    return dotPos == -1 ? null : filename.substring(dotPos+1);
  }
  
  /**
   * 
   */
  public GraphEditorFrame()
  {
    this(null);
  }
  
  /**
   * @param graph 
   * 
   */
  public GraphEditorFrame(Graph<String, Double> graph)
  {
    super("Graph Editor");
    this.graph = (graph == null) ? new Graph<String, Double>() : graph;
    initialize();
  }
  
  /**
   * 
   */
  private void initialize()
  {
    setIconImage(loadImage(ICON_GRAPH));
    
    settingsDialog = new SettingsDialog(this);
    settings = new Settings(new Dimension(800, 600));
    image = new BufferedImage(800, 600, BufferedImage.TYPE_INT_RGB);
    
    randomGraphDialog = new RandomGraphDialog(this);
    randomGraphSettings = new RandomGraphSettings(10, 15);
    
    setMinimumSize(new Dimension(1024, 768));
    
    getContentPane().setLayout(new BorderLayout());
    Container contentPane = getContentPane();
    
    JMenuBar menuBar = new JMenuBar();
    setJMenuBar(menuBar);
    
    JMenu fileMenu;
    menuBar.add(fileMenu = new JMenu("File"));
    
    fileMenu.add(newGraphAction);
    fileMenu.add(loadGraphAction);
    fileMenu.add(saveGraphAction);
    fileMenu.add(saveAsGraphAction);
    
    fileMenu.add(new JSeparator());
    fileMenu.add(exportPNGAction);
    fileMenu.add(randomGraphAction);
    
    fileMenu.add(new JSeparator());
    fileMenu.add(settingsAction);
    
    JMenu verticesMenu;
    menuBar.add(verticesMenu = new JMenu("Vertices"));
    
    verticesMenu.add(createVertexAction);
    verticesMenu.add(deleteVertexAction);
    
    JMenu edgesMenu;
    menuBar.add(edgesMenu = new JMenu("Edges"));
    
    edgesMenu.add(createEdgeAction);
    edgesMenu.add(deleteEdgeAction);
    
    JMenu searchMenu;
    menuBar.add(searchMenu = new JMenu("Search"));
    
    //searchMenu.add(dfsAction);
    searchMenu.add(bfsAction);
    
    toolbar = new JToolBar("Tools", JToolBar.HORIZONTAL);
    contentPane.add(toolbar, BorderLayout.NORTH);
    
    toolbar.add(newGraphAction);
    toolbar.add(loadGraphAction);
    toolbar.add(saveGraphAction);
    toolbar.add(saveAsGraphAction);
    toolbar.add(exportPNGAction);
    toolbar.add(randomGraphAction);
    
    toolbar.add(new JToolBar.Separator());
    toolbar.add(settingsAction);
    
    toolbar.add(new JToolBar.Separator());
    toolbar.add(createVertexAction);
    toolbar.add(deleteVertexAction);
    toolbar.add(createEdgeAction);
    toolbar.add(deleteEdgeAction);
    
    toolbar.add(new JToolBar.Separator());
    //toolbar.add(dfsAction);
    toolbar.add(bfsAction);
    
    centerPanel = new JPanel();
    contentPane.add(centerPanel, BorderLayout.CENTER);
    GridBagLayout gbl_centerPanel = new GridBagLayout();
    gbl_centerPanel.columnWidths = new int[] {150, 150, 0};
    gbl_centerPanel.rowHeights = new int[] {25, 0, 200};
    gbl_centerPanel.columnWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
    gbl_centerPanel.rowWeights = new double[]{0.0, 1.0, 0.0};
    centerPanel.setLayout(gbl_centerPanel);
    
    JLabel lblVertices = new JLabel("Vertices:");
    GridBagConstraints gbc_lblVertices = new GridBagConstraints();
    gbc_lblVertices.anchor = GridBagConstraints.WEST;
    gbc_lblVertices.fill = GridBagConstraints.VERTICAL;
    gbc_lblVertices.insets = new Insets(5, 5, 5, 5);
    gbc_lblVertices.gridx = 0;
    gbc_lblVertices.gridy = 0;
    centerPanel.add(lblVertices, gbc_lblVertices);
    
    JLabel lblEdges = new JLabel("Edges:");
    GridBagConstraints gbc_lblEdges = new GridBagConstraints();
    gbc_lblEdges.anchor = GridBagConstraints.WEST;
    gbc_lblEdges.fill = GridBagConstraints.VERTICAL;
    gbc_lblEdges.insets = new Insets(5, 5, 5, 5);
    gbc_lblEdges.gridx = 1;
    gbc_lblEdges.gridy = 0;
    centerPanel.add(lblEdges, gbc_lblEdges);
    
    vertexList = new JList<>(vertexListModel);
    vertexList.setBorder(new EtchedBorder());
    GridBagConstraints gbc_vertexList = new GridBagConstraints();
    gbc_vertexList.fill = GridBagConstraints.BOTH;
    gbc_vertexList.insets = new Insets(5, 5, 5, 5);
    gbc_vertexList.gridx = 0;
    gbc_vertexList.gridy = 1;
    centerPanel.add(vertexList, gbc_vertexList);
    vertexList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    vertexList.addListSelectionListener(new ListSelectionListener()
    {
      
      @Override
      public void valueChanged(ListSelectionEvent e)
      {
        Graph<String, Double>.Vertex selectedVertex = vertexList.getSelectedValue();
        
        connectedVerticesListModel.clear();
        connectedEdgesListModel.clear();
        
        if (selectedVertex != null)
        {
          Collection<Graph<String, Double>.Vertex> connectedVertices = selectedVertex.getVertices();
          for (Graph<String, Double>.Vertex vertex : connectedVertices)
            connectedVerticesListModel.addElement(vertex);
          
          Set<Graph<String, Double>.Edge> connectedEdges = selectedVertex.getEdges();
          for (Graph<String, Double>.Edge edge : connectedEdges)
            connectedEdgesListModel.addElement(edge);
          
        }
        
      }
      
    });
    
    edgeList = new JList<>(edgeListModel);
    edgeList.setBorder(new EtchedBorder());
    GridBagConstraints gbc_edgeList = new GridBagConstraints();
    gbc_edgeList.fill = GridBagConstraints.BOTH;
    gbc_edgeList.insets = new Insets(5, 5, 5, 5);
    gbc_edgeList.gridx = 1;
    gbc_edgeList.gridy = 1;
    centerPanel.add(edgeList, gbc_edgeList);
    edgeList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    
    graphCanvas = new JLabel();
    graphCanvas.setVerticalAlignment(JLabel.TOP);
    graphCanvas.setHorizontalAlignment(JLabel.LEFT);
    graphCanvas.setIcon(new ImageIcon(image));
    GridBagConstraints gbc_graphCanvas = new GridBagConstraints();
    gbc_graphCanvas.gridheight = 3;
    gbc_graphCanvas.fill = GridBagConstraints.BOTH;
    gbc_graphCanvas.gridx = 2;
    gbc_graphCanvas.gridy = 0;
    if (vertexListModel.getSize() != 0)
      vertexList.setSelectedIndex(0);
    else
      vertexList.setSelectedIndex(-1);
    if (edgeListModel.getSize() != 0)
      edgeList.setSelectedIndex(0);
    else
      edgeList.setSelectedIndex(-1);
    
    JScrollPane scrollPane = new JScrollPane(graphCanvas);
    centerPanel.add(scrollPane, gbc_graphCanvas);
    
    connectedVerticesList = new JList<>(connectedVerticesListModel);
    connectedVerticesList.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
    GridBagConstraints gbc_connectedVerticesList = new GridBagConstraints();
    gbc_connectedVerticesList.anchor = GridBagConstraints.SOUTH;
    gbc_connectedVerticesList.fill = GridBagConstraints.BOTH;
    gbc_connectedVerticesList.insets = new Insets(5, 5, 5, 5);
    gbc_connectedVerticesList.gridx = 0;
    gbc_connectedVerticesList.gridy = 2;
    centerPanel.add(connectedVerticesList, gbc_connectedVerticesList);
    
    connectedEdgesList = new JList<>(connectedEdgesListModel);
    connectedEdgesList.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
    GridBagConstraints gbc_connectedEdgesList = new GridBagConstraints();
    gbc_connectedEdgesList.insets = new Insets(5, 5, 5, 5);
    gbc_connectedEdgesList.fill = GridBagConstraints.BOTH;
    gbc_connectedEdgesList.gridx = 1;
    gbc_connectedEdgesList.gridy = 2;
    centerPanel.add(connectedEdgesList, gbc_connectedEdgesList);
    
    repaintCanvas();
    validate();
  }
  
  @Override
  protected void processComponentEvent(ComponentEvent e)
  {
    super.processComponentEvent(e);
    switch (e.getID())
    {
    case ComponentEvent.COMPONENT_RESIZED:
      centerPanel.validate();
      validate();
      break;
    }
  }
  
  /**
   * 
   */
  private void repaintCanvas()
  {
    Graphics2D g = Graphics2D.class.cast(image.getGraphics());
    Dimension dim = new Dimension(image.getWidth(this), image.getHeight(this));
    
    // draw background
    g.setColor(Color.WHITE);
    g.fillRect(0, 0, dim.width, dim.height);
    
    // align with image dimension
    double scale = (Math.min(dim.width, dim.height) - 50) / 2.0;
    ArrayList<Point2D.Double> points = createCircle(10, vertexListModel.getSize());
    
    // draw edges
    for (int v1 = 0; v1 < vertexListModel.getSize(); v1++)
    {
      Graph<String, Double>.Vertex vertex1 = vertexListModel.getElementAt(v1);
      for (int v2 = 0; v2 < vertexListModel.getSize(); v2++)
      {
        Graph<String, Double>.Vertex vertex2 = vertexListModel.getElementAt(v2);
        Graph<String, Double>.Edge edge = graph.getEdge(vertex1, vertex2);
        
        if ((edge != null) && (edge.getWeight() != 0.0))
        {
          if (v1 == v2) // self connection
          {
            // TODO: implement proper paint code for self connected vertices...
            
            Point2D.Double p = points.get(v1);
            g.setColor(Color.BLACK);
            // draw self connection
            
            g.setColor(Color.BLUE);
            // draw edge string
            
          } else
          {
            Point2D.Double p1 = points.get(v1);
            Point2D.Double p2 = points.get(v2);
            g.setColor(Color.BLACK);
            g.drawLine(
              (int)Math.round((dim.width / 2.0) + (p1.x * scale)),
              (int)Math.round((dim.height / 2.0) + (p1.y * scale)),
              (int)Math.round((dim.width / 2.0) + (p2.x * scale)),
              (int)Math.round((dim.height / 2.0) + (p2.y * scale))
            );
            
            g.setColor(Color.BLUE);
            Rectangle2D rect = g.getFont().getStringBounds(edge.toString(), g.getFontRenderContext());
            g.drawString(edge.toString(), (int)Math.round((dim.width / 2.0) + ((p1.x + p2.x) / 2.0 * scale) - rect.getCenterX()), (int)Math.round((dim.height / 2.0) + (p1.y + p2.y) / 2.0 * scale - rect.getCenterY()));
          }
        }
      }
    }
    
    // draw vertices
    Iterator<Graph<String, Double>.Vertex> iterator = graph.getVertices().iterator();
    for (Point2D.Double point : points)
    {
      Graph<String, Double>.Vertex vertex = iterator.next();
      g.setColor(Color.BLACK);
      g.fillArc((int)Math.round((image.getWidth(this) / 2.0) + (point.x * scale) - 12.5), (int)Math.round((image.getHeight(this) / 2.0) + (point.y * scale) - 12.5), 25, 25, 0, 360);
      g.setColor(Color.RED);
      Rectangle2D rect = g.getFont().getStringBounds(vertex.toString(), g.getFontRenderContext());
      g.drawString(vertex.toString(), (int)Math.round((image.getWidth(this) / 2.0) + (point.x * scale) - rect.getCenterX()), (int)Math.round((image.getHeight(this) / 2.0) + (point.y * scale) - rect.getCenterY()));
    }
    
    // update image icon
    graphCanvas.setIcon(new ImageIcon(image));
  }
  
  /**
   * 
   * @param radius
   * @param numSections
   * @return
   */
  private static ArrayList<Point2D.Double> createCircle(double radius, int numSections)
  {
    ArrayList<Point2D.Double> result = new ArrayList<>(numSections);
    
    double phi = Math.PI * 2.0 / numSections;
    for (int i = 0; i < numSections; i++)
      result.add(new Point2D.Double(Math.sin(phi * i), Math.cos(phi * i)));
    
    return result;
  }
  
  private RandomGraphDialog.RandomGraphSettings queryRandomGraphSettings(RandomGraphDialog.RandomGraphSettings randomGraphSettings)
  {
    return randomGraphDialog.showDialog(randomGraphSettings);
  }
  
  private Settings querySettings(SettingsDialog.Settings settings)
  {
    return settingsDialog.showDialog(settings);
  }
  
  @SuppressWarnings("unchecked")
  private Graph<String, Double>.Vertex queryVertex(String description, Graph<String, Double>.Vertex vertex)
  {
    Collection<Graph<String, Double>.Vertex> verticesCollection = graph.getVertices();
    Object[] verticesArray = verticesCollection.toArray();
    int id = JOptionPane.showOptionDialog(GraphEditorFrame.this, description, "Enter Vertex", JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE, null, verticesArray, vertex);
    return id < 0 ? null : (Graph<String, Double>.Vertex)verticesArray[id];
  }
  
}
