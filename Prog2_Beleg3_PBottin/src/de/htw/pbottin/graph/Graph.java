package de.htw.pbottin.graph;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import de.htw.pbottin.graph.CollectionUtils.Predicate;


/**
 * This class represents an undirected graph with weighted edges.
 * It is implemented using generics so the vertex id type and the weight type can be specified by the programmer.
 * The vertices (nodes) are indexed by a comparable type like String or Integer and can be specified by the V class parameter.
 * The edges are indexed by a pair of vertices and it's weight type is specified by the E class parameter.
 * 
 * @author Paul Bottin
 * 
 * @param <V>
 *  vertex id type
 *  
 * @param <E>
 *  edge weight type
 *  
 */
public class Graph<V extends Comparable<V>, E extends Comparable<E>> implements Serializable
{
  
  /**
   * 
   */
  private static final long serialVersionUID = 8362673764682890589L;

  /**
   * This class represents a vertex in the graph.
   * It is owned by the graph instance so instances of this class have to be created through the graph object's createVertex method.
   * 
   * @author Paul Bottin
   *
   */
  public class Vertex implements Serializable, Comparable<Vertex>
  {
    
    /**
     * 
     */
    private static final long serialVersionUID = 492217723450443922L;

    /**
     * This vertex' unique identifier.
     */
    private final V id;
    
    /**
     * 
     */
    private final Set<Edge> edges = CollectionUtils.createFilteredSet(Graph.this.edges, new Predicate<Edge>() {

      /**
       * 
       */
      private static final long serialVersionUID = 1423578319549936081L;

      @Override
      public boolean apply(Edge value)
      {
        return (value.in.compareTo(id) == 0) || (value.out.compareTo(id) == 0);
      }
      
    });
    
    private final Collection<Vertex> vertices = CollectionUtils.createFilteredCollection(Graph.this.vertices.values(), new Predicate<Vertex>() {

      /**
       * 
       */
      private static final long serialVersionUID = -6721724771374245982L;

      @Override
      public boolean apply(Vertex value)
      {
        boolean result = false;
        
        for (Edge edge : edges)
        {
          if (edge.in.compareTo(Vertex.this.id) == 0)
            result = edge.out.compareTo(value.id) == 0;
          else if (edge.out.compareTo(Vertex.this.id) == 0)
            result = edge.in.compareTo(value.id) == 0;
          
          if (result) break;
        }
        
        return result;
      }
      
    });
    
    /**
     * Creates a new vertex.
     * 
     * @param id
     *  the unique id of this vertex
     */
    private Vertex(V id)
    {
      if (id == null)
        throw new NullPointerException("id cannot be null");
      
      if (Graph.this.getVertex(id) != null)
        throw new IllegalArgumentException("vertex with same id already exists in graph");
      
      this.id = id;
    }
    
    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
      return id.toString();
    }
    
    /*
     * (non-Javadoc)
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    @Override
    public int compareTo(Vertex o)
    {
      return id.compareTo(o.id);
    }
    
    /**
     * Gets all edges connected to this vertex.
     * 
     * @return
     *  a readonly Set view of the edges connected to this vertex
     */
    public Set<Edge> getEdges()
    {
      return this.edges;
    }
    
    /**
     * Gets all vertices connected to this vertex.
     * 
     * @return
     *  a readonly Collection view of the vertices connected to this vertex
     */
    public Collection<Vertex> getVertices()
    {
      return this.vertices;
    }
    
  }
  
  /**
   * This class represents a undirected weighted edge in the graph.
   * It is owned by the graph instance so instances of this class have to be created through the graph object's setEdge methods.
   * 
   * @author Paul Bottin
   *
   */
  public class Edge implements Serializable, Comparable<Edge>
  {
    
    /**
     * 
     */
    private static final long serialVersionUID = 6000721259605482032L;

    /**
     * the first vertex' id
     */
    private final V in;
    
    /**
     * the second vertex' id
     */
    private final V out;
    
    /**
     * the edge weight
     */
    private E weight;
    
    /**
     * Creates a new Edge.
     * 
     * @param in
     *  the first vertex
     *  
     * @param out
     *  the second vertex
     *  
     * @param weight
     *  the edge weight
     *  
     */
    private Edge(V in, V out, E weight)
    {
      if (in == null)
        throw new NullPointerException("in cannot be null");
      
      if (!vertices.containsKey(in))
        throw new IllegalArgumentException("in is not a vertex of this graph");
      
      if (out == null)
        throw new NullPointerException("out cannot be null");
      
      if (!vertices.containsKey(out))
        throw new IllegalArgumentException("out is not a vertex of this graph");
      
      if (weight == null)
        throw new NullPointerException("weight cannot be null");
      
      this.in = in;
      this.out = out;
      this.setWeight(weight);
    }
    
    /**
     * Gets the weight of this edge.
     * 
     * @return
     *  the weight
     */
    public E getWeight()
    {
      return weight;
    }

    /**
     * Sets the weight of this Edge.
     * 
     * @param weight
     *  the weight to set
     */
    public void setWeight(E weight)
    {
      this.weight = weight;
      
      if (weight == null)
        removeEdge(this);
    }
    
    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
      return String.format("(%s, %s) => %s", in, out, weight);
    }
    
    /*
     * (non-Javadoc)
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    @Override
    public int compareTo(Edge o)
    {
      int result;
      
      result = in.compareTo(o.in);
      if (result != 0)
        return result;
      
      result = out.compareTo(o.out);
      if (result != 0)
        return result;
      
      return 0;
    }
    
  }
  
  /**
   * a map of the vertices in this graph
   */
  private final TreeMap<V, Vertex> vertices;
  
  /**
   * a set of the edges in this graph
   */
  private final TreeSet<Edge> edges;
  
  /**
   * Creates a new empty graph instance.
   * 
   */
  public Graph()
  {
    this.vertices = new TreeMap<>();
    this.edges = new TreeSet<>();
  }
  
  /**
   * Creates a new graph and randomly generates vertices and edges in it.
   * 
   * @param rng
   *    a pseudo random number generator
   * 
   * @param numVertices
   *    the number of vertices to generate
   * 
   * @param vertexFactory
   *    the factory object that generates the vertex identifiers
   * 
   * @param numEdges
   *    the number of edges to generate
   * 
   * @param edgeFactory
   *    the factory object that generates the edge weights
   * 
   * @return
   *    the randomly generated graph
   *  
   */
  public static <V extends Comparable<V>, E extends Comparable<E>> Graph<V, E> createRandom(Random rng, int numVertices, VertexFactory<V> vertexFactory, int numEdges, EdgeFactory<V, E> edgeFactory)
  {
    Graph<V, E> graph = new Graph<>();
    ArrayList<Graph<V, E>.Vertex> vertices = new ArrayList<>();
    
    for (int v = 0; v < numVertices; v++)
      vertices.add(graph.createVertex(vertexFactory.createVertex(rng)));
    
    //ArrayList<Graph<V, E>.Edge> edges = new ArrayList<>();
    for (int e = 0; e < numEdges; e++)
    {
      Graph<V, E>.Vertex
        src = vertices.get(rng.nextInt(numVertices)),
        dst = vertices.get(rng.nextInt(numVertices));
      
      while (graph.getEdge(src, dst) != null)
      {
        src = vertices.get(rng.nextInt(numVertices));
        dst = vertices.get(rng.nextInt(numVertices));
      }
      
      graph.setEdge(src, dst, edgeFactory.createEdge(rng, src, dst));
    }
    
    return graph;
  }
  
  /**
   * Creates a new vertex in the graph.
   * 
   * @param id
   *    the id of the new created vertex
   * 
   * @return
   *    the vertex
   * 
   */
  public Vertex createVertex(V id)
  {
    if (vertices.containsKey(id))
      throw new IllegalArgumentException();
    
    Vertex vertex = new Vertex(id);
    vertices.put(id, vertex);
    return vertex;
  }
  
  /**
   * Deletes the given vertex from this graph.
   * Also removes all edges that are using this vertex.
   * 
   * @param vertex
   *    the vertex to delete
   */
  public void deleteVertex(Vertex vertex)
  {
    deleteVertex(vertex.id);
  }
  
  /**
   * Deletes the vertex with the given id from this graph.
   * Also removes all edges that are using this vertex.
   * 
   * @param id
   *    the id of the vertex to delete
   */
  public void deleteVertex(V id)
  {
    if (!vertices.containsKey(id))
      throw new IllegalArgumentException();
    
    vertices.remove(id);
    Iterator<Edge> edgeIterator = edges.iterator();
    while (edgeIterator.hasNext())
    {
      Edge edge = edgeIterator.next();
      if (edge.in.compareTo(id) == 0 || edge.out.compareTo(id) == 0)
        edgeIterator.remove();
    }   
  }
  
  /**
   * Gets a readonly Collection view of the vertices in this graph.
   * 
   * @return
   *    an unmodifyable collection view of the vertices in this graph
   */
  public Collection<Vertex> getVertices()
  {
    return Collections.unmodifiableCollection(vertices.values());
  }
  
  /**
   * Gets a vertex by it's id.
   * 
   * @param id
   *    the id of this vertex
   * 
   * @return
   *    the vertex with the given id or null if there is no vertex with the given id
   */
  public Vertex getVertex(V id)
  {
    Vertex vertex = null;
    
    vertex = vertices.get(id);
    
    return vertex;
  }
  
  /**
   * Creates or modifies an edge in the graph.
   * 
   * @param a
   *    the first vertex' id
   *    
   * @param b
   *    the second vertex' id
   *    
   * @param weight
   *    the edge weight to set<br>
   *    if there's already an edge with the given vertices it will be modified<br>
   *    if weight is null and the edge is exists it will be removed from this graph
   *    
   * @return
   *    the newly created or modified edge object or null if the weight is null
   */
  public Edge setEdge(V a, V b, E weight)
  {
    if (a == null)
      throw new NullPointerException();
    
    if (b == null)
      throw new NullPointerException();
    
    if (!vertices.containsKey(a))
      throw new IllegalArgumentException();
    
    if (!vertices.containsKey(b))
      throw new IllegalArgumentException();
    
    Edge edge = getEdge(a, b);
    if (edge == null)
    {
      if (weight != null)
      {
        edge = new Edge(a, b, weight);
        edges.add(edge);
      }
    } else {
      edge.setWeight(weight);
      if (weight == null)
        edge = null;
    }
    return edge;
  }
  
  /**
   * Creates or modifies an edge in the graph.
   * 
   * @param a
   *    the first vertex
   *    
   * @param b
   *    the second vertex
   *    
   * @param weight
   *    the edge weight to set<br>
   *    if there's already an edge with the given vertices it will be modified<br>
   *    if weight is null and the edge is exists it will be removed from this graph
   *    
   * @return
   *    the newly created or modified edge object or null if the weight is null
   */
  public Edge setEdge(Vertex a, Vertex b, E weight)
  {
    if (a == null)
      throw new NullPointerException();
    if (b == null)
      throw new NullPointerException();
    return setEdge(a.id, b.id, weight);
  }
  
  /**
   * Removes the given edge from this graph.
   * 
   * @param edge
   *    the edge to delete
   */
  public void removeEdge(Edge edge)
  {
    removeEdge(edge.in, edge.out);
  }
  
  /**
   * Removes the edge given by two vertices from this graph.
   * 
   * @param a
   *    the first vertex' id
   *    
   * @param b
   *    the second vertex' id
   *     
   */
  public void removeEdge(V a, V b)
  {
    Iterator<Edge> edgeIterator = edges.iterator();
    while (edgeIterator.hasNext())
    {
      Edge next = edgeIterator.next();
      if ( ( (a.compareTo(next.in) == 0) && (b.compareTo(next.out) == 0) ) || ( (a.compareTo(next.out) == 0) && (b.compareTo(next.in) == 0) ) )
        edgeIterator.remove();
    }
  }
  
  /**
   * Gets the edges in this graph.
   * 
   * @return
   *    an unmodifyable set view of the edges in this graph
   *    
   */
  public Set<Edge> getEdges()
  {
    return Collections.unmodifiableSet(edges);
  }
  
  /**
   * Gets an edge by giving it's vertices.
   * 
   * @param a
   *    the first vertex
   *    
   * @param b
   *    the second vertex
   *    
   * @return
   *    the edge with the given vertices or null if there is no edge with the given vertices
   */
  public Edge getEdge(Vertex a, Vertex b)
  {
    return getEdge(a.id, b.id);
  }
  
  /**
   * Gets an edge by giving it's vertex ids.
   * 
   * @param a
   *    the first vertex' id
   *    
   * @param b
   *    the second vertex' id
   *    
   * @return
   *    the edge with the given vertex ids or null if there is no edge with the given vertices
   */
  public Edge getEdge(V a, V b)
  {
    Edge edge = null;
    
    Iterator<Edge> edgeIterator = edges.iterator();
    while (edgeIterator.hasNext())
    {
      Edge next = edgeIterator.next();
      if ( ( (a.compareTo(next.in) == 0) && (b.compareTo(next.out) == 0) ) || ( (a.compareTo(next.out) == 0) && (b.compareTo(next.in) == 0) ) )
        edge = next;
    }
    
    return edge;
  }
  
  /**
   * Creates a readonly List of edges from the given Path object.
   * 
   * @param path
   *    the Path object as returned by {@link Graph#breadthFirstSearch(Vertex, Vertex)}
   *    
   * @return
   *    a newly created List of all Edge objects in proper ordering that make up the given Path object
   */
  public List<Edge> getVertexPathEdges(Path<Vertex> path)
  {
    LinkedList<Edge> result = new LinkedList<>();
    Iterator<Vertex> pathElements = path.getPathSegments().iterator();
    Vertex prev = null;
    while (pathElements.hasNext())
    {
      Vertex curr = pathElements.next();
      if (prev != null)
      {
        result.add(getEdge(prev, curr));
      }
      prev = curr;
    }
    return Collections.unmodifiableList(result);
  }
  
  /*
   * @param src
   * @param dst
   * @return
   *
  public Set<Path<Vertex>> depthFirstSearch(Vertex src, Vertex dst)
  {
    TreeSet<Path<Vertex>> paths = new TreeSet<>();
    LinkedList<Path<Vertex>> stack = new LinkedList<>();
    stack.addFirst(Path.createRootPath(src));
    
    while (!stack.isEmpty())
    {
      Path<Vertex> currentPath = stack.pollFirst();
      Vertex currentNode = currentPath.getCurrentNode();
      
      if (currentNode.compareTo(dst) == 0)
        paths.add(currentPath);
      
      for (Vertex child : currentNode.getVertices())
        if (!currentPath.getPathSegments().contains(child))
          stack.addFirst(currentPath.getChildPath(child));
    }
    
    return Collections.unmodifiableSet(paths);
  }
  */
  
  /**
   * Performs a breath first search (BFS) to find all possible paths between the <code>src</code> Vertex and the <code>dst</code> Vertex.
   * 
   * @param src
   *    the Vertex where pathfinding should start
   *    
   * @param dst
   *    the target Vertex for pathfinding
   *    
   * @return
   *    a readonly Set of Path objects with all possible paths from the <code>src</code> Vertex to the <code>dst</code> Vertex.
   */
  public Set<Path<Vertex>> breadthFirstSearch(Vertex src, Vertex dst)
  {
    TreeSet<Path<Vertex>> paths = new TreeSet<>();
    LinkedList<Path<Vertex>> queue = new LinkedList<>();
    queue.addLast(Path.createRootPath(src));
    
    while (!queue.isEmpty())
    {
      Path<Vertex> currentPath = queue.pollFirst();
      Vertex currentNode = currentPath.getCurrentSegment();
      
      if (currentNode.compareTo(dst) == 0)
        paths.add(currentPath);
      
      for (Vertex child : currentNode.getVertices())
        if (!currentPath.getPathSegments().contains(child))
          queue.addLast(currentPath.getChildPath(child));
    }
    
    return Collections.unmodifiableSet(paths);
  }
  
  /*
   * (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString()
  {
    StringBuilder sb = new StringBuilder();
    
    sb.append("Vertices:").append('\n');
    for (Vertex v : vertices.values())
      sb.append(v.toString()).append('\n');
    
    sb.append("Edges:").append('\n');
    for (Edge e : edges)
      sb.append(e.toString()).append('\n');
    
    return sb.toString();
  }
  
  /**
   * Writes this Graph instance to the given binary output stream.
   * Flushes the output stream after the last byte has been written but does not close it.
   * 
   * @param out
   *    the previously opened output stream that this Graph object should be serialized to
   *    
   * @throws IOException
   *    on IO errors during the write process
   */
  public void storeGraph(OutputStream out) throws IOException
  {
    ObjectOutputStream oout = new ObjectOutputStream(out);
    oout.writeInt(vertices.size());
    for (V v : vertices.keySet())
    {
      oout.writeObject(v);
    }
    oout.writeInt(edges.size());
    for (Edge edge : edges)
    {
      oout.writeObject(edge.in);
      oout.writeObject(edge.out);
      oout.writeObject(edge.weight);
    }
    oout.flush();
  }
  
  /**
   * Reads a new Graph instance from the given binary input stream.
   * The stream is not closed after the last byte has been read.
   * 
   * @param in
   *    the previously opened input stream where the Graph instance should be deserialized from
   *    
   * @return
   *    a new Graph instance
   * 
   * @throws IOException
   *    on IO errors during the read process
   *    
   * @throws ClassNotFoundException
   *    on deserialization errors
   */
  public static <V extends Comparable<V>, E extends Comparable<E>> Graph<V, E> loadGraph(InputStream in) throws IOException, ClassNotFoundException
  {
    Graph<V, E> graph = new Graph<>();
    
    ObjectInputStream oin = new ObjectInputStream(in);
    int numVertices = oin.readInt();
    while (numVertices > 0)
    {
      @SuppressWarnings("unchecked")
      V id = (V)oin.readObject();
      graph.createVertex(id);
      --numVertices;
    }
    
    int numEdges = oin.readInt();
    while (numEdges > 0)
    {
      @SuppressWarnings("unchecked")
      V a = (V)oin.readObject();
      @SuppressWarnings("unchecked")
      V b = (V)oin.readObject();
      @SuppressWarnings("unchecked")
      E weight = (E)oin.readObject();
      graph.setEdge(a, b, weight);
      --numEdges;
    }
    
    return graph;
  }
}
