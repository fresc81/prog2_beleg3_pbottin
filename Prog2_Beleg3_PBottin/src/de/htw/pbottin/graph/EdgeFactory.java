package de.htw.pbottin.graph;

import java.util.Random;

/**
 * This interface is used in combination with the Graph's createRandom method to randomly generate weighted edges.
 *  
 * @author Paul Bottin
 * 
 * @param <V>
 *    the vertex id type
 *
 * @param <E>
 *    the edge weight type
 */
public interface EdgeFactory<V extends Comparable<V>, E extends Comparable<E>>
{
  
  /**
   * Implementors use this method to randomly generate edges for the graph.
   * 
   * @param rng
   *  a pseudo random number generator
   * 
   * @param src
   *  the first vertex
   *  
   * @param dst
   *  the second vertex
   *  
   * @return
   *  the edge weight
   *  
   */
  E createEdge(Random rng, Graph<V, E>.Vertex src, Graph<V, E>.Vertex dst);
  
}