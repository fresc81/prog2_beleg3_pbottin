package de.htw.pbottin;

import javax.swing.SwingUtilities;

import de.htw.pbottin.graph.ui.GraphEditorFrame;


/**
 * The Graph Editor application main class.
 * 
 * @author Paul Bottin
 *
 */
public class GraphMain
{
  
  private GraphMain()
  {
  }
  
  /**
   * Application entry point.
   * 
   * @param args
   *    commandline arguments
   */
  public static void main(String[] args)
  {
    SwingUtilities.invokeLater(new Runnable()
    {
      
      @Override
      public void run()
      {
        GraphEditorFrame frame = new GraphEditorFrame();
        frame.setDefaultCloseOperation(GraphEditorFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
      }
    });
  }
}
